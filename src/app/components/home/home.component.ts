import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
 customOptions: OwlOptions = {
  loop: true,
  margin: 0,
  nav: true,
  items: 1,
  dots: false,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
  smartSpeed: 1200,
  autoHeight: false,
  autoplay: true,
 }
}
